/*
XPAGES pour WebJaxe

Copyright (C) 2007 Observatoire de Paris-Meudon

Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier conform�ment aux dispositions de la Licence Publique G�n�rale GNU, telle que publi�e par la Free Software Foundation ; version 2 de la licence, ou encore (� votre choix) toute version ult�rieure.

Ce programme est distribu� dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans m�me la garantie implicite de COMMERCIALISATION ou D'ADAPTATION A UN OBJET PARTICULIER. Pour plus de d�tail, voir la Licence Publique G�n�rale GNU .

Vous devez avoir re�u un exemplaire de la Licence Publique G�n�rale GNU en m�me temps que ce programme ; si ce n'est pas le cas, �crivez � la Free Software Foundation Inc., 675 Mass Ave, Cambridge, MA 02139, Etats-Unis.
*/

package xpages;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.*;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.text.Position;

import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.Text;

import jaxe.JaxeDocument;
import jaxe.JaxeElement;
import jaxe.JaxeResourceBundle;
import jaxe.equations.DialogueEquation;


/**
 * Equation dont l'image n'est pas enregistr�e dans un fichier, mais gard�e en m�moire et
 * copi�e en base64 � l'int�rieur de l'�l�ment XML.
 * Type d'�l�ment Jaxe: 'equation'
 * param�tre: texteAtt: le nom de l'attribut donnant le texte de l'�quation
 * param�tre: labelAtt: le nom de l'attribut donnant le label de l'image
 */
public class JEEquationMemoire extends JaxeElement {
    JLabel label = null;
    public float alignementY;
    private EcouteSouris ecouteur;
    
    public JEEquationMemoire(final JaxeDocument doc) {
        this.doc = doc;
        alignementY = (float)0.70;
        // 70% du composant au-dessus de la base de la ligne
        // donc pas parfait (il faudrait se placer par rapport au milieu vertical d'un 'x'
        // et non par rapport � la base de la ligne pour obtenir un r�sulat parfait,
        // mais ce n'est pas possible avec Swing)
    }

    @Override
    public void init(final Position pos, final Node noeud) {
        final Element el = (Element)noeud;
        final Element refElement = doc.cfg.getElementRef(el);
        
        String contenu = null;
        final Node nTexte = el.getFirstChild();
        if (nTexte != null && nTexte instanceof Text)
            contenu = nTexte.getNodeValue();
        
        BufferedImage img;
        if (contenu == null || "".equals(contenu))
            img = null;
        else
            img = lireImage(contenu);
        
        if (img == null) {
            label = new JLabel(getString("erreur.AffichageImage"));
            label.setBorder(BorderFactory.createLineBorder(Color.darkGray));
        } else {
            ImageIcon icon;
            if (img != null) {
                icon = new ImageIcon(img);
                final int hauteur = img.getHeight(null);
                if (hauteur != -1)
                    alignementY = (float)((hauteur/2.0 + 4) / hauteur);
            } else
                icon = null;
            if (icon == null || icon.getImageLoadStatus() == MediaTracker.ABORTED ||
                icon.getImageLoadStatus() == MediaTracker.ERRORED) {
                label = new JLabel(getString("erreur.AffichageImage"));
                label.setBorder(BorderFactory.createLineBorder(Color.darkGray));
            } else
                label = new JLabel(icon);
        }
        label.setAlignmentY(alignementY);
        
        ecouteur = new EcouteSouris(this, doc.jframe);
        label.addMouseListener(ecouteur);
        final Position newpos = insertComponent(pos, label);
    }
    
    /**
     * Lecture de l'image de l'�quation au format PNG encod� en base64 dans l'�l�ment
     */
    protected BufferedImage lireImage(String contenu) {
        try {
            return(ImageIO.read(new DecodeurBase64(new StringReader(contenu))));
        } catch (IOException ex) {
            System.err.println("lireImage(String): IOException: " + ex.getMessage());
            return(null);
        }
    }
    
    @Override
    public Node nouvelElement(final Element refElement) {
        final String texteAtt = doc.cfg.valeurParametreElement(refElement, "texteAtt", null);
        final String labelAtt = doc.cfg.valeurParametreElement(refElement, "labelAtt", null);
        
        final Element newel = nouvelElementDOM(doc, refElement);
        if (newel == null)
            return null;
        
        final DialogueEquation dlg = new DialogueEquation(doc, "", labelAtt, null);
        if (!dlg.afficher())
            return null;
        final String texte = dlg.getTexte();
        enregistrerImage(newel, DialogueEquation.creerImage(texte));
        final String valeurLabel = dlg.getLabel();
        
        try {
            newel.setAttributeNS(doc.cfg.espaceAttribut(texteAtt), texteAtt, texte);
            if (labelAtt != null && valeurLabel != null && !"".equals(valeurLabel))
                newel.setAttributeNS(doc.cfg.espaceAttribut(labelAtt), labelAtt, valeurLabel);
        } catch (final DOMException ex) {
            System.err.println("nouvelElement(Element): DOMException: " + ex.getMessage());
            return null;
        }

        return newel;
    }
    
    @Override
    public void afficherDialogue(final JFrame jframe) {
        final Element el = (Element)noeud;

        final Element refElement = doc.cfg.getElementRef(el);
        final String texteAtt = doc.cfg.valeurParametreElement(refElement, "texteAtt", null);
        final String labelAtt = doc.cfg.valeurParametreElement(refElement, "labelAtt", null);
        String texteEquation = el.getAttribute(texteAtt);
        String valeurLabel1;
        if (labelAtt != null)
            valeurLabel1 = el.getAttribute(labelAtt);
        else
            valeurLabel1 = null;
        final DialogueEquation dlg = new DialogueEquation(doc, texteEquation, labelAtt, valeurLabel1);
        if (!dlg.afficher())
            return;
        texteEquation = dlg.getTexte();
        enregistrerImage(el, DialogueEquation.creerImage(texteEquation));
        final String valeurLabel2 = dlg.getLabel();
        
        try {
            el.setAttributeNS(doc.cfg.espaceAttribut(texteAtt), texteAtt, texteEquation);
            if (labelAtt != null) {
                if (valeurLabel1 != null && "".equals(valeurLabel2))
                    el.removeAttribute(labelAtt);
                else
                    el.setAttributeNS(doc.cfg.espaceAttribut(labelAtt), labelAtt, valeurLabel2);
            }
        } catch (final DOMException ex) {
            System.err.println("afficherDialogue(JFrame): DOMException: " + ex.getMessage());
            return;
        }
        doc.modif = true;
        
        majAffichage();
    }
    
    @Override
    public void majAffichage() {
        String contenu = null;
        final Element el = (Element)noeud;
        final Node nTexte = el.getFirstChild();
        if (nTexte != null && nTexte instanceof Text)
            contenu = nTexte.getNodeValue();
        if (contenu != null && !"".equals(contenu)) {
            Image img = lireImage(contenu);
            boolean erreur = false;
            if (img == null)
                erreur = true;
            if (!erreur) {
                final ImageIcon icon = new ImageIcon(img);
                label.setIcon(icon);
                label.setText(null);
                label.setBorder(null);
                final int hauteur = img.getHeight(null);
                if (hauteur != -1) {
                    alignementY = (float)((hauteur/2.0 + 4) / hauteur);
                    label.setAlignmentY(alignementY);
                }
            } else {
                label.setIcon(null);
                label.setText(getString("erreur.AffichageImage"));
                label.setBorder(BorderFactory.createLineBorder(Color.darkGray));
            }
        } else {
            label.setText(getString("erreur.FichierNonTrouve"));
            label.setIcon(null);
            label.setBorder(BorderFactory.createLineBorder(Color.darkGray));
        }
        doc.imageChanged(label);
    }

    /**
     * Enregistrement de l'image de l'�quation au format PNG encod� en base64 dans l'�l�ment
     */
    protected void enregistrerImage(final Element el, final BufferedImage img) {
        try {
            StringWriter sw = new StringWriter();
            EncodeurBase64 encodeur = new EncodeurBase64(sw);
            DialogueEquation.enregistrerImage(img, encodeur);
            encodeur.close();
            Node nTexte = el.getFirstChild();
            if (nTexte == null) {
                nTexte = doc.DOMdoc.createTextNode(sw.toString());
                el.appendChild(nTexte);
            } else
                nTexte.setNodeValue(sw.toString());
        } catch (final IOException ex) {
            System.err.println("enregistrerImage(Element, BufferedImage): IOException: " + ex.getMessage());
            JOptionPane.showMessageDialog(doc.jframe, JaxeResourceBundle.getRB().getString("erreur.Enregistrement") + ": " +
                ex.getMessage(), JaxeResourceBundle.getRB().getString("erreur.Erreur"), JOptionPane.ERROR_MESSAGE);
        }
    }
    
    @Override
    public void selection(final boolean select) {
        super.selection(select);
        label.setEnabled(!select);
    }
    
    @Override
    public void effacer() {
        super.effacer();
        if (ecouteur != null) {
            label.removeMouseListener(ecouteur);
            ecouteur = null;
        }
    }

    class EcouteSouris extends MouseAdapter {
        JEEquationMemoire jei;
        JFrame jframe;
        public EcouteSouris(final JEEquationMemoire obj, final JFrame jframe) {
            super();
            jei = obj;
            this.jframe = jframe;
        }
        @Override
        public void mouseClicked(final MouseEvent e) {
            jei.afficherDialogue(jframe);
        }
    }
}
