/*
XPAGES pour WebJaxe

Copyright (C) 2007 Observatoire de Paris-Meudon

Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier conform�ment aux dispositions de la Licence Publique G�n�rale GNU, telle que publi�e par la Free Software Foundation ; version 2 de la licence, ou encore (� votre choix) toute version ult�rieure.

Ce programme est distribu� dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans m�me la garantie implicite de COMMERCIALISATION ou D'ADAPTATION A UN OBJET PARTICULIER. Pour plus de d�tail, voir la Licence Publique G�n�rale GNU .

Vous devez avoir re�u un exemplaire de la Licence Publique G�n�rale GNU en m�me temps que ce programme ; si ce n'est pas le cas, �crivez � la Free Software Foundation Inc., 675 Mass Ave, Cambridge, MA 02139, Etats-Unis.
*/

package xpages;

import java.io.File;

/**
  * usine � ImAnim
  */
public class ImAnimFactory {
	public static ImAnim create(File f) {
		String fname = f.getName().toLowerCase();
		if (fname.endsWith(".png"))
			return(new PNGImage());
		else if (fname.endsWith(".jpeg") || fname.endsWith(".jpg"))
			return(new JPEGImage());
		else if (fname.endsWith(".gif"))
			return(new GIFAnim());
		else if (fname.endsWith(".mpeg") || fname.endsWith(".mpg"))
			return(new MPEGAnim());
		else if (fname.endsWith(".ogg") || fname.endsWith(".ogv"))
			return(new OggAnim());
		else
			return(null);
	}
}
