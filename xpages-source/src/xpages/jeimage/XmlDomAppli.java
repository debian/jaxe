/*
Jaxe - Editeur XML en Java

Copyright (C) 2010 Observatoire de Paris-Meudon

Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier conform�ment aux dispositions de la Licence Publique G�n�rale GNU, telle que publi�e par la Free Software Foundation ; version 2 de la licence, ou encore (� votre choix) toute version ult�rieure.

Ce programme est distribu� dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans m�me la garantie implicite de COMMERCIALISATION ou D'ADAPTATION A UN OBJET PARTICULIER. Pour plus de d�tail, voir la Licence Publique G�n�rale GNU .

Vous devez avoir re�u un exemplaire de la Licence Publique G�n�rale GNU en m�me temps que ce programme ; si ce n'est pas le cas, �crivez � la Free Software Foundation Inc., 675 Mass Ave, Cambridge, MA 02139, Etats-Unis.
*/

package xpages.jeimage;

import org.apache.log4j.Logger;

import javax.xml.parsers.*;

import org.w3c.dom.*;

import java.io.File;


/**
 * Cr�e un document DOM � partir du contenu d'un dossier
 */
public class XmlDomAppli {
    /**
     * Logger for this class
     */
    private static final Logger LOG = Logger.getLogger(XmlDomAppli.class);
    
    private static final String[] extension = new String []{"jpg","jpeg","png","gif","ogv","ogg","mpg","mpeg"};
    
    private Document docXMLappli;
    private Element racine;
    
    public XmlDomAppli(File cheminC) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            docXMLappli = builder.newDocument();
            racine = docXMLappli.createElement("DOSSIER");
            docXMLappli.appendChild(racine);
            racine.setAttribute("nom",cheminC.getName());
            creerDOM(cheminC, racine);            
        }
        catch (Exception ex) {
            LOG.error("newDocumentBuilder()", ex);  
        }
    }
    
    
    /**
     * retourne le Document XML
     */
    public Document getDocXMLappli() {
        return docXMLappli;
    }
    
    
    /**
     * cr�e le document DOM � partir du contenu du dossier cheminC
     * le DOM est de niveau 1 (sans espace de nom)
     * l'arborescence du DOM est compos�e d'une racine (DOSSIER) et des noeuds fils (DOSSIER et FICHIER)
     */
    private void creerDOM(File cheminC, Element parent) {
        try {
            File[] f = cheminC.listFiles();
            
            for (int i=0; i<f.length; i++) {
                             
                if ( f[i].isFile() ) {
                    for (String ext: extension) {
                        if ( f[i].toString().endsWith("."+ext) || f[i].toString().endsWith("."+ext.toUpperCase()) )
                            ajoutNoeudF(parent,f[i].getName());
                    }
                }
                    
                if ( f[i].isDirectory() ) {
                    Element Noeud = ajoutNoeudD(parent,f[i].getName());
                    creerDOM(f[i], Noeud);
                }
            }
        }
        catch (Exception ex) {
            LOG.error("creerDOM(File)", ex); 
        }
    }
    
    
    /**
     * cr�e un �l�ment DOSSIER
     */
    private static Element ajoutNoeudD(Element parent, String valeur) {
        Element element = parent.getOwnerDocument().createElement("DOSSIER");
        element.setAttribute("nom",valeur);
        parent.appendChild(element);
        return element;
   }
    
    
    /**
     * cr�e un �l�ment FICHIER
     */
    private static Element ajoutNoeudF(Element parent, String valeur) {
        Element element = parent.getOwnerDocument().createElement("FICHIER");
        element.setAttribute("nom",valeur);
        parent.appendChild(element);
        return element;
   }
   
}
