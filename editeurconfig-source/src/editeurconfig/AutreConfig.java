/*
editeurconfig - Editeur de Fichiers de Config de Jaxe

Copyright (C) 2011 Observatoire de Paris-Meudon

Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier conform�ment aux dispositions de la Licence Publique G�n�rale GNU, telle que publi�e par la Free Software Foundation ; version 2 de la licence, ou encore (� votre choix) toute version ult�rieure.

Ce programme est distribu� dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans m�me la garantie implicite de COMMERCIALISATION ou D'ADAPTATION A UN OBJET PARTICULIER. Pour plus de d�tail, voir la Licence Publique G�n�rale GNU .

Vous devez avoir re�u un exemplaire de la Licence Publique G�n�rale GNU en m�me temps que ce programme ; si ce n'est pas le cas, �crivez � la Free Software Foundation Inc., 675 Mass Ave, Cambridge, MA 02139, Etats-Unis.
*/

package editeurconfig;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.tree.*;
import javax.swing.border.*;
import java.util.*;
import org.w3c.dom.*;
import javax.swing.text.BadLocationException;
import javax.swing.filechooser.FileFilter;
import java.io.*;
import java.net.*;


/**
* Classe pour cr�er ou �diter autres configuration
*/
public class AutreConfig extends JFrame {
	private FenetreEdition fe;
    private Document doc;
    private Element racine;
    private Element langage;
    private Element autre_config;
    private File fichierXML;
    
    JTextField nomFichier_t;
    
    private JPanel listePanelAutreConfig;
    
    
    public AutreConfig(final FenetreEdition fe, final Document doc, final Element racine, final File fichierXML) {
        super(Strings.get("titre.AutreConfig"));
        this.fe = fe;
        this.doc = doc;
        this.racine = racine;
        this.fichierXML = fichierXML;
        
        langage = Outils.premierEnfantDeNom(racine, "LANGAGE");
        
        autre_config = Outils.premierEnfantDeNom(langage, "AUTRE_CONFIG");
        if (autre_config == null) {
            autre_config = doc.createElement("AUTRE_CONFIG");
            langage.appendChild(autre_config);
        }
        
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        afficher();
    }
    
    
    private void afficher() {
        
        setLayout(new BorderLayout());
        
        listePanelAutreConfig = new JPanel();
        listePanelAutreConfig.setLayout(new BoxLayout(listePanelAutreConfig, BoxLayout.Y_AXIS));
        JScrollPane defilement = new JScrollPane(listePanelAutreConfig);
        defilement.setPreferredSize(new Dimension(800,400));
        add(defilement,BorderLayout.NORTH);
        
        recupererAutreConfig();
        
        final JPanel panelBoutonsBas = new JPanel(new FlowLayout());
        add(panelBoutonsBas,BorderLayout.SOUTH);
        final JButton bTester = new JButton(new AbstractAction("Tester") {
            public void actionPerformed(ActionEvent e) {
                EditeurFichierConfig.convertToString(doc);
            }
        });
        //panelBoutonsBas.add(bTester);
        
        final JButton bFermer = new JButton(new AbstractAction(Strings.get("bouton.Fermer")) {
            public void actionPerformed(ActionEvent e) {
                nettoyerDOM();
                setVisible(false);
                fe.afficher();
            }
        });
        panelBoutonsBas.add(bFermer);
        
        pack();
        final Dimension dim = getSize();
        final Dimension ecran = getToolkit().getScreenSize();
        setLocation((ecran.width - dim.width)/2, (ecran.height - dim.height)/2);
        setVisible(true);
    }
        

    /**
    * Cr�e le panel Autre Config
    */
    private void creerPanelAutreConfig(final Element autre_config, final int index) {
        
        final JPanel panAutreConfigTout = new JPanel(new FlowLayout(FlowLayout.LEFT));
        listePanelAutreConfig.add(panAutreConfigTout);
        
        final JPanel panAutreConfig = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panAutreConfigTout.add(panAutreConfig);
        JLabel nomFichier = new JLabel(Strings.get("label.AutreConfig"));
        nomFichier_t = new JTextField(15);
        nomFichier_t.addMouseListener(new MouseListener() {
            public void mouseClicked(MouseEvent e) {
                parcourir(autre_config);
            }
            public void mousePressed(MouseEvent e) {
            }
            public void mouseReleased(MouseEvent e) {
            }
            public void mouseEntered(MouseEvent e) {
            }
            public void mouseExited(MouseEvent e) {
            }
        });
        
        JButton bParcourir = new JButton(new AbstractAction(Strings.get("bouton.Parcourir")) {
            public void actionPerformed(ActionEvent e) {
                    parcourir(autre_config);
            }
        });
        panAutreConfig.add(nomFichier);
        panAutreConfig.add(nomFichier_t);
        panAutreConfig.add(bParcourir);
        
        if (autre_config != null)
            nomFichier_t.setText(autre_config.getAttribute("nom"));
            
        // �couteur sur nom de fichier
        nomFichier_t.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent de) {
                ecouteurAutreConfig(de, index);
            }
            public void insertUpdate(DocumentEvent de) {
                ecouteurAutreConfig(de, index);
            }
            public void removeUpdate(DocumentEvent de) {
                ecouteurAutreConfig(de, index);
            }
        });
        
        
        final JPanel panPlusMoins = new JPanel(new FlowLayout());
        panAutreConfigTout.add(panPlusMoins);
        
        final JButton bPlus = new JButton(new AbstractAction("+") {
            public void actionPerformed(ActionEvent e) {
                    Element element_autre_config = enregistrerAutreConfig(listePanelAutreConfig.getComponentCount(), null);
                    creerPanelAutreConfig(element_autre_config, listePanelAutreConfig.getComponentCount());
                    listePanelAutreConfig.revalidate();
            }
        });
        panPlusMoins.add(bPlus);
        
        final JButton bMoins = new JButton(new AbstractAction("-") {
            public void actionPerformed(ActionEvent e) {
                if (listePanelAutreConfig.getComponentCount() > 1) {
                    listePanelAutreConfig.remove(panAutreConfigTout);
                    listePanelAutreConfig.revalidate();
                    Element element_autre_config = trouverAutreConfig(index);
                    if (element_autre_config != null) {
                        element_autre_config.getParentNode().removeChild(element_autre_config);
                        EditeurFichierConfig.setModif(true);
                    }
                }
                if (listePanelAutreConfig.getComponentCount() == 1)
                    nomFichier_t.setText(null);
            }
        });
        panPlusMoins.add(bMoins);
    }
    
    
    
    /**
    * Parcour un fichier XML
    */
    private void parcourir(final Element autre_config) {
        File repertoireSchema = null;
        if ( ! "".equals(autre_config.getAttribute("nom")) )
            repertoireSchema = new File(fichierXML.getParentFile(), autre_config.getAttribute("nom"));
        else 
            repertoireSchema = fichierXML;
        
        JFileChooser jfc = new JFileChooser(repertoireSchema);
        jfc.setApproveButtonText("OK");
        if ( ! "".equals(autre_config.getAttribute("nom")) )
            jfc.setSelectedFile(repertoireSchema);
		jfc.setFileFilter(new FileFilter() {
			public boolean accept(File f) {
				if (f.isDirectory())
					return true;
				String s = f.getName();
				int i = s.lastIndexOf('.');

				if (i > 0 && i < s.length() - 1)
					if (s.substring(i + 1).toLowerCase().equals("xml"))
						return true;

				return false;
			}

			public String getDescription() {
                return Strings.get("texte.FichiersXML");
			}
		});
		
        if (jfc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
            final File sf = jfc.getSelectedFile();
            if (sf != null) {
                URI url = null;
                URI base = null;
                try {
                    url = new URI(sf.toString());
                }
                catch (URISyntaxException e) {
                    System.out.println(e);
                }
                
                try {
                    base = new URI(fichierXML.getParentFile().toString());
                }
                catch (URISyntaxException e) {
                    System.out.println(e);
                }
                
                try {
                    URI relative = base.relativize(url);
                    nomFichier_t.setText(relative.toString());
                }
                catch (NullPointerException e) {
                    System.out.println(e);
                }
            }
        }
    }
    
    
    /*********************************** Ecouteurs ************************************************/
    /**
    * Ecouteur sur le champ autre config
    * @param Un DocumentEvent
    */
    private void ecouteurAutreConfig(final DocumentEvent de, final int index) {
        javax.swing.text.Document source = de.getDocument();
        try {
            String texteNom = source.getText(0, source.getLength());
            enregistrerAutreConfig(index, texteNom);
            
            EditeurFichierConfig.setModif(true);
        }
        catch (BadLocationException bex) {
            System.out.println(bex);
        }
    }    
    
    
    
    /*********************************** Op�ration DOM *****************************************/    
    // AUTRE_CONFIG
    private Element enregistrerAutreConfig(final int index, final String nomF) {
        Element autre_config = trouverAutreConfig(index);
        if (autre_config == null) {
            autre_config = doc.createElement("AUTRE_CONFIG");
            langage.appendChild(autre_config);
        }
        if (nomF != null)
            autre_config.setAttribute("nom", nomF);
        return autre_config;
    }
    
    private Element trouverAutreConfig(int index) {
        if (index == -1)
            return(null);
        if (langage == null)
            return(null);
        int ie = 0;
        for (Node n = langage.getFirstChild(); n != null; n = n.getNextSibling()) {
            if (n instanceof Element && "AUTRE_CONFIG".equals(n.getNodeName())) {
                if (ie == index)
                    return((Element)n);
                ie++;
            }
        }
        return(null);
    }
    
    
    
    /**
    * Remplie le(s) panel(s) Autre config � partir du DOM
    */
    private void recupererAutreConfig() {
        listePanelAutreConfig.removeAll();
        Element autre_config = null;
        if (langage != null) {
            autre_config = Outils.premierEnfantDeNom(langage, "AUTRE_CONFIG");
            if (autre_config == null)
                autre_config = enregistrerAutreConfig(0, null);
            
            creerPanelAutreConfig(autre_config, 0);
            
            int enfants = langage.getChildNodes().getLength();
            for (int i = 1; i < enfants ; i++) {
                autre_config = Outils.enfantSuivantDeMemeNom(autre_config);
                if (autre_config != null)
                    creerPanelAutreConfig(autre_config, i);
            }
        }
    }


    private void nettoyerDOM() {
        for (Node n = langage.getFirstChild(); n != null; n = n.getNextSibling()) {
            if (n instanceof Element && "AUTRE_CONFIG".equals(n.getNodeName())) {
                if (!((Element)n).hasAttribute("nom") || "".equals(((Element)n).getAttribute("nom")))
                    langage.removeChild(n);
            }
        }
    }
}
