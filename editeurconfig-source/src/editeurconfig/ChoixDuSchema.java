/*
editeurconfig - Editeur de Fichiers de Config de Jaxe

Copyright (C) 2011 Observatoire de Paris-Meudon

Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier conform�ment aux dispositions de la Licence Publique G�n�rale GNU, telle que publi�e par la Free Software Foundation ; version 2 de la licence, ou encore (� votre choix) toute version ult�rieure.

Ce programme est distribu� dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans m�me la garantie implicite de COMMERCIALISATION ou D'ADAPTATION A UN OBJET PARTICULIER. Pour plus de d�tail, voir la Licence Publique G�n�rale GNU .

Vous devez avoir re�u un exemplaire de la Licence Publique G�n�rale GNU en m�me temps que ce programme ; si ce n'est pas le cas, �crivez � la Free Software Foundation Inc., 675 Mass Ave, Cambridge, MA 02139, Etats-Unis.
*/

package editeurconfig;

import javax.swing.*;
import javax.swing.event.*;
import org.w3c.dom.*;
import java.awt.event.*;
import java.awt.*;


/**
* Classe pour choisir un sch�ma: XML Schema, RelaxNG ou Schema XML simplifi�
*/
public class ChoixDuSchema extends JDialog {
    
    private String schemaSelectionne = "";
    
	private EditeurFichierConfig appl;
    private Document doc;
    private Element racine;
    
    private JList listeSchemas;
    private JButton bOK;
    
    public ChoixDuSchema(final EditeurFichierConfig appl, final Document doc, final Element racine) {
        super(new JFrame(), Strings.get("titre.ChoixDuSchema"));
        this.appl = appl;
        this.doc = doc;
        this.racine = racine;
        
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        afficher();
    }
    
    
    /**
    * Affiche dans un dialogue la liste des sch�mas
    */
    private void afficher() {
        
        JLabel texte = new JLabel(Strings.get("label.choixSchema"));
        add(texte, BorderLayout.NORTH);
        
        JPanel panelBoutons = new JPanel();
        
        choisir();
        
        add(new JScrollPane(listeSchemas), BorderLayout.CENTER);
        
        listeSchemas.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent evt) { 
                if (listeSchemas.getSelectedValue() != null) { 
                    schemaSelectionne = listeSchemas.getSelectedValue().toString();
                    bOK.setEnabled(true);
                }
            }
        });
        
        
        listeSchemas.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    setVisible(false);
                    if (schemaSelectionne == Strings.get("texte.XMLSchema"))
                        new ParcourirSchema(appl, doc, racine, "xsd");
                    if (schemaSelectionne == Strings.get("texte.RelaxNG"))
                        new ParcourirSchema(appl, doc, racine, "rng");
                    if (schemaSelectionne == Strings.get("texte.SchemaXMLSimplifie")) {
                        setVisible(true);
                        JOptionPane.showMessageDialog(ChoixDuSchema.this, Strings.get("message.SchemaPasPrisEnCharge"));//new EditeurSchemaSimple(); 
                    }
                }
            }
        });
        
        
        final JButton bAnnuler = new JButton(new AbstractAction(Strings.get("bouton.Annuler")) {
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                new DefinirLaLangue(appl, doc, racine);
            }
        });
        panelBoutons.add(bAnnuler);
        
        bOK = new JButton(new AbstractAction(Strings.get("bouton.OK")) {
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                if (schemaSelectionne == Strings.get("texte.XMLSchema"))
                    new ParcourirSchema(appl, doc, racine, "xsd");
                if (schemaSelectionne == Strings.get("texte.RelaxNG"))
                    new ParcourirSchema(appl, doc, racine, "rng");
                if (schemaSelectionne == Strings.get("texte.SchemaXMLSimplifie")) {
                    setVisible(true);
                    JOptionPane.showMessageDialog(ChoixDuSchema.this, Strings.get("message.SchemaPasPrisEnCharge"));//new EditeurSchemaSimple(); 
                }
            }
        });
        panelBoutons.add(bOK);
        bOK.setEnabled(false);
        
        add(panelBoutons, BorderLayout.SOUTH);
        pack();
        final Dimension dim = getSize();
        final Dimension ecran = getToolkit().getScreenSize();
        setLocation((ecran.width - dim.width)/2, (ecran.height - dim.height)/2);
        setVisible(true);
    }
        
    
    private void choisir() {
        DefaultListModel model = new DefaultListModel();
        listeSchemas = new JList(model);
        listeSchemas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        model.addElement(Strings.get("texte.XMLSchema"));
        model.addElement(Strings.get("texte.RelaxNG"));
        model.addElement(Strings.get("texte.SchemaXMLSimplifie"));
    }
    
}


