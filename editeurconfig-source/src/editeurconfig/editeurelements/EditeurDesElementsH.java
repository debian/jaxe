/*
editeurconfig - Editeur de Fichiers de Config de Jaxe

Copyright (C) 2011 Observatoire de Paris-Meudon

Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier conform�ment aux dispositions de la Licence Publique G�n�rale GNU, telle que publi�e par la Free Software Foundation ; version 2 de la licence, ou encore (� votre choix) toute version ult�rieure.

Ce programme est distribu� dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans m�me la garantie implicite de COMMERCIALISATION ou D'ADAPTATION A UN OBJET PARTICULIER. Pour plus de d�tail, voir la Licence Publique G�n�rale GNU .

Vous devez avoir re�u un exemplaire de la Licence Publique G�n�rale GNU en m�me temps que ce programme ; si ce n'est pas le cas, �crivez � la Free Software Foundation Inc., 675 Mass Ave, Cambridge, MA 02139, Etats-Unis.
*/

package editeurconfig;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.tree.*;
import javax.swing.border.*;
import java.util.*;


/**
* Classe qui affiche dans un JPanel la liste des �l�ments du sch�ma
*/
public class EditeurDesElementsH extends JPanel {

    private ListeElements listeElements;
    
    
    public EditeurDesElementsH(final ListeElements listeElements) {
        this.listeElements = listeElements;
        afficher();
    }
    
    private void afficher() {
        setLayout(new BorderLayout());
        
        listeElements.JListeDesElements().setModel(listeElements.getListModel());
        final JScrollPane defilementListe = new JScrollPane(listeElements.JListeDesElements());
        defilementListe.setPreferredSize(new Dimension(50, 200));
        JTextField texteRecherche = listeElements.getJTexteRecherche();
        texteRecherche.setText("");
        add(texteRecherche, BorderLayout.NORTH);
        add(defilementListe, BorderLayout.CENTER);
    }
}
