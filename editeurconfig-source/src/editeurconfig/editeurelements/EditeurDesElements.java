/*
editeurconfig - Editeur de Fichiers de Config de Jaxe

Copyright (C) 2011 Observatoire de Paris-Meudon

Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier conform�ment aux dispositions de la Licence Publique G�n�rale GNU, telle que publi�e par la Free Software Foundation ; version 2 de la licence, ou encore (� votre choix) toute version ult�rieure.

Ce programme est distribu� dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans m�me la garantie implicite de COMMERCIALISATION ou D'ADAPTATION A UN OBJET PARTICULIER. Pour plus de d�tail, voir la Licence Publique G�n�rale GNU .

Vous devez avoir re�u un exemplaire de la Licence Publique G�n�rale GNU en m�me temps que ce programme ; si ce n'est pas le cas, �crivez � la Free Software Foundation Inc., 675 Mass Ave, Cambridge, MA 02139, Etats-Unis.
*/

package editeurconfig;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.tree.*;
import javax.swing.border.*;
import java.util.*;
import org.w3c.dom.*;


/**
* Classe principale de l'Editeur des El�ments, est compos�e de 2 classes d�rivant de JPanel
*/
public class EditeurDesElements extends JFrame {
    
    private FenetreEdition fe;
    private Document doc;
    private Element racine;
    private ListeElements listeElements;
    
    private EditeurDesElementsH eDEH;
    private EditeurDesElementsB eDEB;
    
    public EditeurDesElements(final FenetreEdition fe, final Document doc, final Element racine, final ListeElements listeElements) {
        super(Strings.get("titre.EditeurDesElements"));
        this.fe = fe;
        this.doc = doc;
        this.racine = racine;
        this.listeElements = listeElements;
        
        eDEH = new EditeurDesElementsH(listeElements);
        eDEB = new EditeurDesElementsB(listeElements, fe, this, doc, racine);
        
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        
        afficher();
    }
        
    private void afficher() {
                
        add(eDEH, BorderLayout.NORTH);
        add(eDEB);    
        
        pack();
        final Dimension dim = getSize();
        final Dimension ecran = getToolkit().getScreenSize();
        setLocation((ecran.width - dim.width)/2, (ecran.height - dim.height)/2);
		setVisible(true);
    }
    
}
