/*
editeurconfig - Editeur de Fichiers de Config de Jaxe

Copyright (C) 2011 Observatoire de Paris-Meudon

Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier conform�ment aux dispositions de la Licence Publique G�n�rale GNU, telle que publi�e par la Free Software Foundation ; version 2 de la licence, ou encore (� votre choix) toute version ult�rieure.

Ce programme est distribu� dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans m�me la garantie implicite de COMMERCIALISATION ou D'ADAPTATION A UN OBJET PARTICULIER. Pour plus de d�tail, voir la Licence Publique G�n�rale GNU .

Vous devez avoir re�u un exemplaire de la Licence Publique G�n�rale GNU en m�me temps que ce programme ; si ce n'est pas le cas, �crivez � la Free Software Foundation Inc., 675 Mass Ave, Cambridge, MA 02139, Etats-Unis.
*/

package editeurconfig;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.tree.*;
import javax.swing.border.*;
import java.util.*;
import org.w3c.dom.*;
import javax.swing.text.BadLocationException;


/**
* Classe qui affiche dans un JPanel les champs nec�ssaires pour les �l�ments
*/
public class EditeurDesElementsB extends JPanel {
    
    private Node monNoeud;
    
    private ListeElements listeElements;
    private FenetreEdition fe;
    private EditeurDesElements ede;
    private Document doc;
    private Element racine;
    private Element affichage_noeuds;
    private Element affichageElementSelectionne;
    
    private static final String[] type_element_dom = {"division", "zone", "string", "vide", "tableau", 
                                                "tabletexte", "typesimple", "style", "fichier", "symbole", 
                                                "liste", "item", "equation", "formulaire", "plugin"};
                                                
    private static final String[] type_element = {Strings.get("label.division"), Strings.get("label.zone"), Strings.get("label.string"), Strings.get("label.vide"), 
                                                Strings.get("label.tableau"), Strings.get("label.tabletexte"), Strings.get("label.typesimple"), 
                                                Strings.get("label.style"), Strings.get("label.fichier"), Strings.get("label.symbole"), Strings.get("label.liste"),
                                                Strings.get("label.item"), Strings.get("label.equation"), Strings.get("label.formulaire"), 
                                                Strings.get("label.plugin")};
                                                
     
    private String elementSelectionne = "";
    private String attributSelectionne = "";
    private ArrayList<String> listeAttributs = new ArrayList<String>();
    
    private JTextField nomElement_t;
    private JComboBox typeElement_c;
    private JTextField valeurSuggeree_t;
    private JComboBox nomAttribut_c;
    private JTextField nomParametre_t;
    private JTextField valeurParametre_t;
    private JButton bEditerTxt;
    private JButton bEditerTxtAffAtt;
    
    private JPanel panelAffichage;
    private JPanel panelBoutons;
    private JPanel listePanelValeurSuggeree;
    private JPanel listePanelAffichageAttribut;
    private JPanel listePanelValeurSuggereeA;
    private JPanel listePanelParametre;
    
	private JList listeEl;
    private DefaultComboBoxModel comboModel;
    
    private JButton bMoins;

                    
    /**
    * R�cup�re les �l�ments du sch�ma et les enregistre dans le DOM
    */
    public EditeurDesElementsB(final ListeElements listeElements, final FenetreEdition fe, final EditeurDesElements ede, final Document doc, final Element racine) {
        this.listeElements = listeElements;
        this.fe = fe;
        this.ede = ede;
        this.doc = doc;
        this.racine = racine;

		listeEl = listeElements.JListeDesElements();
        
        affichage_noeuds = Outils.premierEnfantDeNom(racine, "AFFICHAGE_NOEUDS");
        
        afficher();
    }
    
    
    private void afficher() {
        
        listeEl.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent evt) {                    
                if (listeEl.getSelectedValue() != null) {
                    if (listeEl.getSelectedValue() instanceof InfosNoeud) {
                        InfosNoeud infosNoeud = (InfosNoeud)listeEl.getSelectedValue();
                        monNoeud = infosNoeud.getNoeud();
                    }
                        
                    bEditerTxt.setEnabled(true);
                    typeElement_c.setEnabled(true);
                    listePanelValeurSuggeree.setVisible(true);
                    listePanelParametre.setVisible(true);
                    elementSelectionne = listeEl.getSelectedValue().toString();
                    affichageElementSelectionne = Outils.getElementSelectionne(affichage_noeuds, "AFFICHAGE_ELEMENT", elementSelectionne, "element");
                
                    //listeElements.getJTexteRecherche().setText(elementSelectionne);
                    nomElement_t.setText(elementSelectionne);
                        
                    // mise � jour du type d'�l�ment
                    ecouteurTypeElement();
                    
                    // remplir champ valeur sugg�r�e de l'�l�ment
                    recupererValeurSuggeree(listePanelValeurSuggeree, affichageElementSelectionne);
                    
                    // si le typesimple ou plugin est selectionn�, on active le champ valeur sugg�r�e
                    if (typeElement_c.getSelectedIndex() == 6 || typeElement_c.getSelectedIndex() == 14)
                        activerComposant(listePanelValeurSuggeree, true);
                    else 
                        activerComposant(listePanelValeurSuggeree, false);
            
                    // afficher panel Affichage d'attribut si l'�l�ment a des attributs
                    if (listeAttributs != null) 
                        listeAttributs.clear();
                    listeAttributs = listeElements.getAttributElement(listeElements.getReferenceElement(elementSelectionne));
                    if (listeAttributs != null) {
                        // remplir champ Affichage d'attribut
                        recupererAffichageAttribut();
                        //listePanelAffichageAttribut.setVisible(true);
                        listePanelAffichageAttribut.setVisible(true);
                    }
                    else 
                        listePanelAffichageAttribut.setVisible(false);
                    
                    // remplir champ parametre
                    recupererParametre();
                }
                else {
                    nomElement_t.setText("");
                    bEditerTxt.setEnabled(false);
                    typeElement_c.setEnabled(false);
                    listePanelValeurSuggeree.setVisible(false);
                    listePanelAffichageAttribut.setVisible(false);
                    listePanelParametre.setVisible(false);
                }
            }
        });
            
        
        setLayout(new BorderLayout());
        setPreferredSize(new Dimension(700,700));
        
        creerPanelBoutons();
        
        /***********************  Panel Affichage *******************/
        panelAffichage = new JPanel();
        panelAffichage.setLayout(new BoxLayout(panelAffichage, BoxLayout.Y_AXIS));
        panelAffichage.setBorder(new TitledBorder(Strings.get("label.Affichage")));
        final JScrollPane defilementAffichage = new JScrollPane(panelAffichage);
        
        // champs �l�ment, type
        final JPanel panElement = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panelAffichage.add(panElement);
        JLabel nomElement = new JLabel(Strings.get("label.NomElement"));
        nomElement_t = new JTextField(15);
        nomElement_t.setEditable(false);
        panElement.add(nomElement);
        panElement.add(nomElement_t);
        panElement.add(bEditerTxt);
        
        final JPanel panType = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panelAffichage.add(panType);
        JLabel typeElement = new JLabel(Strings.get("label.TypeE"));
        typeElement_c = new JComboBox(type_element);
        typeElement_c.setSelectedIndex(2);
        typeElement_c.setEnabled(false);
            
        panType.add(typeElement);
        panType.add(typeElement_c);
        
        
        // champs valeur sugg�r�e pour l'�l�ment
        listePanelValeurSuggeree = new JPanel();
        listePanelValeurSuggeree.setLayout(new BoxLayout(listePanelValeurSuggeree, BoxLayout.Y_AXIS));
        panelAffichage.add(listePanelValeurSuggeree);
        
        // panel Affichage d'attribut
        listePanelAffichageAttribut = new JPanel();
        listePanelAffichageAttribut.setLayout(new BoxLayout(listePanelAffichageAttribut, BoxLayout.Y_AXIS));
        panelAffichage.add(listePanelAffichageAttribut);
        
        // panel Param�tre
        listePanelParametre = new JPanel();
        listePanelParametre.setLayout(new BoxLayout(listePanelParametre, BoxLayout.Y_AXIS));
        panelAffichage.add(listePanelParametre);
        
        add(defilementAffichage,BorderLayout.CENTER);
        add(panelBoutons,BorderLayout.SOUTH);
        
    }// fin afficher   
    
    
    /**
    * Cr�e le panel Boutons
    */
    private void creerPanelBoutons() {
        panelBoutons = new JPanel(new FlowLayout());
        
        final JButton bTester = new JButton(new AbstractAction("Tester") {
            public void actionPerformed(ActionEvent e) {
                EditeurFichierConfig.convertToString(doc);
            }
        });
        //panelBoutons.add(bTester);
        
        final JButton bFermer = new JButton(new AbstractAction(Strings.get("bouton.Fermer")) {
            public void actionPerformed(ActionEvent e) {
                nettoyerDOM();
                ede.setVisible(false);
                fe.afficher();
            }
        });
        panelBoutons.add(bFermer);
        
        bEditerTxt = new JButton(new AbstractAction(Strings.get("bouton.EditerTextes")) {
            public void actionPerformed(ActionEvent e) {
                editerTextesE();
            }
        });
        bEditerTxt.setEnabled(false);
    }
    
    
    
    /**
    * Cr�e le panel Valeur Sugg�r�e
    * @param Le panel destination
    * @param L'�l�ment parent
    * @param l'�l�ment valeur_suggeree
    * @param index
    */
    private void creerPanelValeurSuggeree(final JPanel panelDest, final Element parent, final Element valeur_suggeree, final int index) {
        final JPanel panValeurSuggeree = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panelDest.add(panValeurSuggeree);
        JLabel valeurSuggeree = new JLabel(Strings.get("label.valeurSuggeree"));
        valeurSuggeree_t = new JTextField(15);
        if (valeur_suggeree != null)
            valeurSuggeree_t.setText(Outils.getValeurElement(valeur_suggeree));
        panValeurSuggeree.add(valeurSuggeree);
        panValeurSuggeree.add(valeurSuggeree_t);
        
        // �couteur sur valeur suggeree
        valeurSuggeree_t.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent de) {
                ecouteurValeurSuggeree(de, parent, index);
            }
            public void insertUpdate(DocumentEvent de) {
                ecouteurValeurSuggeree(de, parent, index);
            }
            public void removeUpdate(DocumentEvent de) {
                ecouteurValeurSuggeree(de, parent, index);
            }
        });
        
        final JPanel panPlusMoins = new JPanel(new FlowLayout());
        panValeurSuggeree.add(panPlusMoins);
        
        final JButton boutonPlus = new JButton(new AbstractAction("+") {
            public void actionPerformed(ActionEvent e) {
                //if (!("".equals(valeurSuggeree_t.getText()))) {
                    creerPanelValeurSuggeree(panelDest, parent, null, panelDest.getComponentCount());
                    panelDest.revalidate();
                //}
            }
        });
        panPlusMoins.add(boutonPlus);
        
        final JButton boutonMoins = new JButton(new AbstractAction("-") {
            public void actionPerformed(ActionEvent e) {
                if (panelDest.getComponentCount() > 1) {
                    panelDest.remove(panValeurSuggeree);
                    panelDest.revalidate();
                    Element element_valeur_suggeree = trouverValeurSuggeree(parent, index);
                    if (element_valeur_suggeree != null)
                        element_valeur_suggeree.getParentNode().removeChild(element_valeur_suggeree);
                }
            }
        });
        panPlusMoins.add(boutonMoins);
    }
    
    
    
    /**
    * Cr�e le panel Affichage Attribut
    * @param l'�l�ment affichage_attribut
    * @param index
    */
    private void creerPanelAffichageAttribut(final Element affichage_attribut, final int index) {
        final JPanel panAffichageAttributTout = new JPanel(new FlowLayout(FlowLayout.LEFT));
        listePanelAffichageAttribut.add(panAffichageAttributTout);
        
        final JPanel panAffichageAttribut = new JPanel();
        panAffichageAttributTout.add(panAffichageAttribut);
        panAffichageAttribut.setLayout(new BoxLayout(panAffichageAttribut, BoxLayout.Y_AXIS));
        panAffichageAttribut.setBorder(new TitledBorder(Strings.get("label.AffichageAttribut")));
        
        final JPanel panAttribut = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panAffichageAttribut.add(panAttribut);
        JLabel nomAttribut = new JLabel(Strings.get("label.nomAttribut"));
        nomAttribut_c = new JComboBox();
        comboModel = new DefaultComboBoxModel(listeAttributs.toArray());
        nomAttribut_c.setModel(comboModel);
        nomAttribut_c.setSelectedIndex(0);
        if (affichage_attribut != null)
            nomAttribut_c.setSelectedItem(affichage_attribut.getAttribute("attribut"));
        panAttribut.add(nomAttribut);
        panAttribut.add(nomAttribut_c);
        
        // �couteur sur attribut
        nomAttribut_c.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent ie) {
                if (nomAttribut_c.getSelectedItem() != null) {
                    attributSelectionne = nomAttribut_c.getSelectedItem().toString();
                    enregistrerAffichageAttribut(index, attributSelectionne);
                }
            }
        });
        
        listePanelValeurSuggereeA = new JPanel();
        listePanelValeurSuggereeA.setLayout(new BoxLayout(listePanelValeurSuggereeA, BoxLayout.Y_AXIS));
        panAffichageAttribut.add(listePanelValeurSuggereeA);
        
        //final Element element_affichage_attribut = enregistrerAffichageAttribut(index, null);
        if (affichage_attribut == null)
            recupererValeurSuggeree(listePanelValeurSuggereeA, enregistrerAffichageAttribut(index, null));
        else
            recupererValeurSuggeree(listePanelValeurSuggereeA, affichage_attribut);
        
        bEditerTxtAffAtt = new JButton(new AbstractAction(Strings.get("bouton.EditerTextes")) {
            public void actionPerformed(ActionEvent e) {
                //editerTextesAttribut(affichage_attribut.getAttribute("attribut"));
                editerTextesAttribut(nomAttribut_c.getSelectedItem().toString());
            }
        });
        panAffichageAttribut.add(bEditerTxtAffAtt);
        
        final JPanel panPlusMoins = new JPanel(new FlowLayout());
        panAffichageAttributTout.add(panPlusMoins);
        
        final JButton boutonPlus = new JButton(new AbstractAction("+") {
            public void actionPerformed(ActionEvent e) {
                if (listeAttributs.size() > 1) {
                    //System.out.println("listeAttributs avant: "+listeAttributs);
                    listeAttributs.remove(nomAttribut_c.getSelectedItem());
                    //System.out.println("listeAttributs apres: "+listeAttributs);
                    creerPanelAffichageAttribut(null, listePanelAffichageAttribut.getComponentCount());
                    listePanelAffichageAttribut.revalidate();
                }
            }
        });
        panPlusMoins.add(boutonPlus);
        
        final JButton boutonMoins = new JButton(new AbstractAction("-") {
            public void actionPerformed(ActionEvent e) {
                if (listePanelAffichageAttribut.getComponentCount() > 1) {
                    listeAttributs.add(nomAttribut_c.getSelectedItem().toString());
                    //System.out.println("listeAttributs apres: "+listeAttributs);
                    listePanelAffichageAttribut.remove(panAffichageAttributTout);
                    listePanelAffichageAttribut.revalidate();
                    Element element_affichage_attribut = trouverAffichageAttribut(index);
                    if (element_affichage_attribut != null)
                        element_affichage_attribut.getParentNode().removeChild(element_affichage_attribut);
                }
            }
        });
        panPlusMoins.add(boutonMoins);
    }
    
    
    
    
    /**
    * Cr�e le panel Parametre
    * @param l'�l�ment parametre
    * @param index
    */
    private void creerPanelParametre(final Element parametre, final int index) {
        
        final JPanel panParametreTout = new JPanel(new FlowLayout(FlowLayout.LEFT));
        listePanelParametre.add(panParametreTout);
        
        final JPanel panParametre = new JPanel(new GridLayout(2,1));
        panParametreTout.add(panParametre);
        panParametre.setBorder(new TitledBorder(Strings.get("label.Parametre")));
        
        final JPanel panNomParametre = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panParametre.add(panNomParametre);
        JLabel nomParametre = new JLabel(Strings.get("label.Nom"));
        nomParametre_t = new JTextField(15);
        if (parametre != null)
            nomParametre_t.setText(parametre.getAttribute("nom"));
        panNomParametre.add(nomParametre);
        panNomParametre.add(nomParametre_t);
        
        // �couteur sur nom parametre
        nomParametre_t.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent de) {
                ecouteurParametre(de, index, "nom");
            }
            public void insertUpdate(DocumentEvent de) {
                ecouteurParametre(de, index, "nom");
            }
            public void removeUpdate(DocumentEvent de) {
                ecouteurParametre(de, index, "nom");
            }
        });
        
        final JPanel panValeurParametre = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panParametre.add(panValeurParametre);
        JLabel valeurParametre = new JLabel(Strings.get("label.valeurParametre"));
        valeurParametre_t = new JTextField(15);
        if (parametre != null)
            valeurParametre_t.setText(parametre.getAttribute("valeur"));
        panValeurParametre.add(valeurParametre);
        panValeurParametre.add(valeurParametre_t);
        
        // �couteur sur valeur parametre
        valeurParametre_t.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent de) {
                ecouteurParametre(de, index, "valeur");
            }
            public void insertUpdate(DocumentEvent de) {
                ecouteurParametre(de, index, "valeur");
            }
            public void removeUpdate(DocumentEvent de) {
                ecouteurParametre(de, index, "valeur");
            }
        });
        
        final JPanel panPlusMoins = new JPanel(new FlowLayout());
        panParametreTout.add(panPlusMoins);
        
        final JButton bPlus = new JButton(new AbstractAction("+") {
            public void actionPerformed(ActionEvent e) {
                if (!("".equals(nomParametre_t.getText()))) {
                    if (!("".equals(valeurParametre_t.getText()))) {
                        //bMoins.setEnabled(false);
                        creerPanelParametre(null, listePanelParametre.getComponentCount());
                        listePanelParametre.revalidate();
                    }
                }
            }
        });
        panPlusMoins.add(bPlus);
        
        bMoins = new JButton(new AbstractAction("-") {
            public void actionPerformed(ActionEvent e) {
                if (listePanelParametre.getComponentCount() > 1) {
                    listePanelParametre.remove(panParametreTout);
                    listePanelParametre.revalidate();
                    Element element_parametre = trouverParametre(index);
                    if (element_parametre != null)
                        element_parametre.getParentNode().removeChild(element_parametre);
                }
            }
        });
        panPlusMoins.add(bMoins);
    }
    
    
    /**
    * Mise � jour du champs Type element, modifie l'attribut dans le DOM
    */
    private void ecouteurTypeElement() {
        typeElement_c.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent ie) {
                if (typeElement_c.getSelectedItem() != null) {
                    affichageElementSelectionne.setAttribute("type", type_element_dom[typeElement_c.getSelectedIndex()]);
                    
                    EditeurFichierConfig.setModif(true);
                    
                    if (typeElement_c.getSelectedIndex() == 6 || typeElement_c.getSelectedIndex() == 14)
                        activerComposant(listePanelValeurSuggeree, true);
                    else 
                        activerComposant(listePanelValeurSuggeree, false);
                }
            }
        });
        if (affichageElementSelectionne != null)
            typeElement_c.setSelectedItem(Strings.get("label."+affichageElementSelectionne.getAttribute("type")));
    }
    
    
    /**
    * Ecouteur sur le champ Valeur sugg�r�e
    * @param Un DocumentEvent
    * @param L'index
    */
    private void ecouteurValeurSuggeree(final DocumentEvent de, final Element parent, final int index) {
        javax.swing.text.Document source = de.getDocument();
        try {
            String texteValeurS = source.getText(0, source.getLength());
            enregistrerValeurSuggeree(parent, index, texteValeurS);
            
            EditeurFichierConfig.setModif(true);
        }
        catch (BadLocationException bex) {
            System.out.println(bex);
        }
    }
    
    
    
    /**
    * Ecouteur sur le champ Parametre
    * @param Un DocumentEvent
    * @param L'index
    * @param le champ d�sign�
    */
    private void ecouteurParametre(final DocumentEvent de, final int index, final String champP) {
        javax.swing.text.Document source = de.getDocument();
        try {
            String texteP = source.getText(0, source.getLength());
            if ("nom".equals(champP))
                enregistrerParametre(index, texteP, null);
            if ("valeur".equals(champP))
                enregistrerParametre(index, null, texteP);
                
            EditeurFichierConfig.setModif(true);
        }
        catch (BadLocationException bex) {
            System.out.println(bex);
        }
    }
    
    
    
    
    /**
    * Edite les textes d'un �l�ment
    * @param Le document doc
    * @param L'�l�ment s�lectionn� sous forme de String
    */
    private void editerTextesE() {
        new TextesElement(doc, elementSelectionne);
    }
    
    /**
    * Edite les textes d'un affichage attribut
    * @param Le document doc
    * @param L'�l�ment s�lectionn� sous forme de String
    */
    private void editerTextesAttribut(final String attr) {
        if (attr == null)
            JOptionPane.showMessageDialog(this, Strings.get("message.SelectionnezUnAttribut"));
        else 
            new TextesAttribut(doc, affichageElementSelectionne, attr);
    }
    
    /*********************************** Op�ration DOM *****************************************/
    // VALEUR_SUGGEREE
    private Element enregistrerValeurSuggeree(final Element parent, final int index, final String valeurS) {
        Element valeur_suggeree = trouverValeurSuggeree(parent, index);
        if (valeur_suggeree == null) {
            valeur_suggeree = doc.createElement("VALEUR_SUGGEREE");
            parent.appendChild(valeur_suggeree);
        }
        if (valeurS != null)
            Outils.setValeurElement(doc, valeur_suggeree, valeurS);
        
        return valeur_suggeree;
    }
    
    private Element trouverValeurSuggeree(final Element parent, final int index) {
        if (index == -1)
            return(null);
        if (parent == null)
            return(null);
        int iv = 0;
        for (Node n = parent.getFirstChild(); n != null; n = n.getNextSibling()) {
            if (n instanceof Element && "VALEUR_SUGGEREE".equals(n.getNodeName())) {
                if (iv == index)
                    return((Element)n);
                iv++;
            }
        }
        return(null);
    }
    
    
    // AFFICHAGE_ATTRIBUT
    private Element enregistrerAffichageAttribut(final int index, final String attribut) {
        Element affichage_attribut = trouverAffichageAttribut(index);
        if (affichage_attribut == null) {
            affichage_attribut = doc.createElement("AFFICHAGE_ATTRIBUT");
            affichageElementSelectionne.appendChild(affichage_attribut);
        }
        if (attribut != null)
            affichage_attribut.setAttribute("attribut", attribut);
        else
            affichage_attribut.setAttribute("attribut", listeAttributs.get(0));
        
        return affichage_attribut;
    }
    
    private Element trouverAffichageAttribut(final int index) {
        if (index == -1)
            return(null);
        if (affichageElementSelectionne == null)
            return(null);
        int ia = 0;
        for (Node n = affichageElementSelectionne.getFirstChild(); n != null; n = n.getNextSibling()) {
            if (n instanceof Element && "AFFICHAGE_ATTRIBUT".equals(n.getNodeName())) {
                if (ia == index)
                    return((Element)n);
                ia++;
            }
        }
        return(null);
    }
    
    
    // PARAMETRE
    private Element enregistrerParametre(final int index, final String nomP, final String valeurP) {
        Element parametre = trouverParametre(index);
        if (parametre == null) {
            parametre = doc.createElement("PARAMETRE");
            affichageElementSelectionne.appendChild(parametre);
        }
        if (nomP != null)
            parametre.setAttribute("nom", nomP);
        if (valeurP != null)
            parametre.setAttribute("valeur", valeurP);
        
        return parametre;
    }
    
    private Element trouverParametre(int index) {
        if (index == -1)
            return(null);
        if (affichageElementSelectionne == null)
            return(null);
        int ip = 0;
        for (Node n = affichageElementSelectionne.getFirstChild(); n != null; n = n.getNextSibling()) {
            if (n instanceof Element && "PARAMETRE".equals(n.getNodeName())) {
                if (ip == index)
                    return((Element)n);
                ip++;
            }
        }
        return(null);
    }
             
                    
    
    /**
    * Remplie le(s) champ(s) Valeur Sugg�r�e � partir du DOM
    */
    private void recupererValeurSuggeree(final JPanel panelDest, final Element parent) {
        panelDest.removeAll();

        if (parent != null) {
            Element valeur_suggeree = Outils.premierEnfantDeNom(parent, "VALEUR_SUGGEREE");
            if (valeur_suggeree == null)
                valeur_suggeree = enregistrerValeurSuggeree(parent, 0, null);
            
            creerPanelValeurSuggeree(panelDest, parent, valeur_suggeree, 0);
            
            int enfants = parent.getChildNodes().getLength();
            for (int i = 1; i < enfants ; i++) {
                valeur_suggeree = Outils.enfantSuivantDeMemeNom(valeur_suggeree);
                if (valeur_suggeree != null)
                    creerPanelValeurSuggeree(panelDest, parent, valeur_suggeree, i);
            }
        }
        panelDest.revalidate();
    }
    
    /**
    * Remplie le(s) champ(s) Affichage Attribut � partir du DOM
    */
    private void recupererAffichageAttribut() {
        listePanelAffichageAttribut.removeAll();

        if (affichageElementSelectionne != null) {
            Element affichage_attribut = Outils.premierEnfantDeNom(affichageElementSelectionne, "AFFICHAGE_ATTRIBUT");
            if (affichage_attribut == null) 
                affichage_attribut = enregistrerAffichageAttribut(0, null);
                
            creerPanelAffichageAttribut(affichage_attribut, 0);
            
            listeAttributs.remove(affichage_attribut.getAttribute("attribut"));
            
            int enfants = affichageElementSelectionne.getChildNodes().getLength();
            for (int i = 1; i < enfants ; i++) {
                affichage_attribut = Outils.enfantSuivantDeMemeNom(affichage_attribut);
                if (affichage_attribut != null) {
                    creerPanelAffichageAttribut(affichage_attribut, i);
                    listeAttributs.remove(affichage_attribut.getAttribute("attribut"));
                }
            }
            
        }
    }
    
    /**
    * Remplie le(s) champ(s) Parametre � partir du DOM
    */
    private void recupererParametre() {
        listePanelParametre.removeAll();
    
        if (affichageElementSelectionne != null) {
            Element parametre = Outils.premierEnfantDeNom(affichageElementSelectionne, "PARAMETRE");
            if (parametre == null)
                parametre = enregistrerParametre(0, null, null);
                
            creerPanelParametre(parametre,0);
        
            int enfants = affichageElementSelectionne.getChildNodes().getLength();
            for (int i = 1; i < enfants ; i++) {
                parametre = Outils.enfantSuivantDeMemeNom(parametre);
                if (parametre != null)
                    creerPanelParametre(parametre, i);
            }
        }
    }
    
    
    /**
    * Active ou d�sactive un composant graphique
    * @param Le composant JComponent
    * @param b, true pour activer, false pour d�sactiver
    */
    private void activerComposant(JComponent composant, boolean b) {
        composant.setEnabled(b) ;
        for (int i = 0 ; i < composant.getComponentCount() ; i++) {
            Component c = composant.getComponent(i) ;
            if (c instanceof JComponent) {
                activerComposant((JComponent) c, b) ;
            }
        }
    }
    
    private void nettoyerDOM() {
        for (Node n = affichage_noeuds.getFirstChild(); n != null; n = n.getNextSibling())
            if (n instanceof Element && "AFFICHAGE_ELEMENT".equals(n.getNodeName())) {
                // PARAMETRE
                for (Node node = n.getFirstChild(); node != null; node = node.getNextSibling())
                    if ( "PARAMETRE".equals(node.getNodeName()) && (!((Element)node).hasAttribute("nom") || !((Element)node).hasAttribute("valeur") || "".equals(((Element)node).getAttribute("nom")) || "".equals(((Element)node).getAttribute("valeur"))) )
                        n.removeChild(node);

                // VALEUR_SUGGEREE
                for (Node node1 = n.getFirstChild(); node1 != null; node1 = node1.getNextSibling())
                    if ("VALEUR_SUGGEREE".equals(node1.getNodeName()) && (!node1.hasChildNodes() || "".equals(Outils.getValeurElement((Element)node1))))
                        n.removeChild(node1);

                // AFFICHAGE_ATTRIBUT
                for (Node node2 = n.getFirstChild(); node2 != null; node2 = node2.getNextSibling())
                    if ("AFFICHAGE_ATTRIBUT".equals(node2.getNodeName())) {
                        if (!((Element)node2).hasAttribute("attribut")) //|| "".equals(((Element)node).getAttribute("attribut")) )
                            n.removeChild(node2);
                            
                        for (Node node22 = node2.getFirstChild(); node22 != null; node22 = node22.getNextSibling())
                            if ("VALEUR_SUGGEREE".equals(node22.getNodeName()) && (!node22.hasChildNodes()))
                                node2.removeChild(node22);
                    }
            }
    }

}
