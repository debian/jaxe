/*
editeurconfig - Editeur de Fichiers de Config de Jaxe

Copyright (C) 2011 Observatoire de Paris-Meudon

Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier conform�ment aux dispositions de la Licence Publique G�n�rale GNU, telle que publi�e par la Free Software Foundation ; version 2 de la licence, ou encore (� votre choix) toute version ult�rieure.

Ce programme est distribu� dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans m�me la garantie implicite de COMMERCIALISATION ou D'ADAPTATION A UN OBJET PARTICULIER. Pour plus de d�tail, voir la Licence Publique G�n�rale GNU .

Vous devez avoir re�u un exemplaire de la Licence Publique G�n�rale GNU en m�me temps que ce programme ; si ce n'est pas le cas, �crivez � la Free Software Foundation Inc., 675 Mass Ave, Cambridge, MA 02139, Etats-Unis.
*/

package editeurconfig;

import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;
import org.w3c.dom.*;
import java.io.*;
import java.util.*;


/**
* Classe � partir de laquelle on peut s�l�ctionner une racine, 
* �diter les Menus, �diter les El�ments, �diter les Exports, �diter les Strings
*/
public class FenetreEdition extends JFrame {
        
    private EditeurFichierConfig appl;
    private File fichierXML;
    private Document doc;
    private Element racine;
    private Element affichage_noeuds;
    private ListeElements listeElements;
    
    private EditeurDeMenus editeurM;
    private EditeurDesElements editeurE;
    private EditeurDesTextes editeurT;
    
    private ArrayList<String> lesElements = new ArrayList<String>();
    
    
    public FenetreEdition(final EditeurFichierConfig appl, final File fichierXML, final Document doc, final Element racine, final ListeElements listeElements) {
        super(Strings.get("titre.FenetreEdition"));
        this.appl = appl;
        this.fichierXML = fichierXML;
        this.doc = doc;
        this.racine = racine;
        this.listeElements = listeElements;
        
        affichage_noeuds = Outils.premierEnfantDeNom(racine, "AFFICHAGE_NOEUDS");
        if (affichage_noeuds == null) {
            affichage_noeuds = doc.createElement("AFFICHAGE_NOEUDS");
            racine.insertBefore(affichage_noeuds, Outils.premierEnfantDeNom(racine, "STRINGS"));
        }
        
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        afficher();
        
        lesElements = listeElements.getLesElements();
        for (int i = 0; i < lesElements.size(); i++)
            enregistrerAffichageElement(lesElements.get(i).toString());
    }
    
    public void afficher() {
        
        setLayout(new BorderLayout());    
        
        final JPanel panelBoutons = new JPanel();
        panelBoutons.setLayout(new BoxLayout(panelBoutons, BoxLayout.Y_AXIS));
        panelBoutons.add(Box.createVerticalStrut(40));
        
        // bouton racine
        final JButton bRacine = new JButton(new AbstractAction(Strings.get("bouton.Racines")) {
            public void actionPerformed(ActionEvent e) {
                new ChoixRacine(doc, racine, listeElements);
            }
        });
        panelBoutons.add(bRacine);
        panelBoutons.add(Box.createVerticalStrut(15));
        
        // bouton Autre configuration
        final JButton bAutreConfig = new JButton(new AbstractAction(Strings.get("bouton.AutreConfig")) {
            public void actionPerformed(ActionEvent e) {
                autreConfig();
            }
        });
        panelBoutons.add(bAutreConfig);
        panelBoutons.add(Box.createVerticalStrut(15));
        
        // bouton enregistrement
        final JButton bEnregistrement = new JButton(new AbstractAction(Strings.get("bouton.Enregistrement")) {
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                editerEnregistrement();
            }
        });
        panelBoutons.add(bEnregistrement);
        panelBoutons.add(Box.createVerticalStrut(15));
        
        // bouton �diteur de menus
        final JButton bEditeurMenus = new JButton(new AbstractAction(Strings.get("bouton.EditeurDeMenus")) {
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                editerMenus();
            }
        });
        panelBoutons.add(bEditeurMenus);
        panelBoutons.add(Box.createVerticalStrut(15));
        
        // bouton �diteur des �l�ments
        final JButton bEditeurElements = new JButton(new AbstractAction(Strings.get("bouton.EditeurDesElements")) {
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                editerElements();
            }
        });
        panelBoutons.add(bEditeurElements);
        panelBoutons.add(Box.createVerticalStrut(15));
        
        // bouton �diteur des exports
        final JButton bEditeurExports= new JButton(new AbstractAction(Strings.get("bouton.EditeurDesExports")) {
            public void actionPerformed(ActionEvent e) {
                editerExports();
            }
        });
        panelBoutons.add(bEditeurExports);
        panelBoutons.add(Box.createVerticalStrut(15));
        
        // bouton �diteur des textes g�n�ral
        final JButton bEditeurTextesG = new JButton(new AbstractAction(Strings.get("bouton.EditeurDesTextesG")) {
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                editerTextesG();
            }
        });
        panelBoutons.add(bEditeurTextesG);
        panelBoutons.add(Box.createVerticalStrut(50));
        
        final JPanel panelBas = new JPanel(new FlowLayout());
        
        final JButton bQuitter = new JButton(new AbstractAction(Strings.get("bouton.Quitter")) {
            public void actionPerformed(ActionEvent e) {
                if (appl.getModif()) {
                    final int r = JOptionPane.showConfirmDialog(FenetreEdition.this, Strings.get("message.EnregistrerAvant"), Strings.get("titre.Fermeture"), JOptionPane.YES_NO_CANCEL_OPTION);
                    if (r == JOptionPane.NO_OPTION)
                        appl.quitter();
                    else if (r == JOptionPane.YES_OPTION) {
                        if (appl.enregistrerFichier(fichierXML)) {
                            JOptionPane.showMessageDialog(FenetreEdition.this, Strings.get("message.EnregistrementReussi"));
                            setVisible(false);
                            new EditeurFichierConfig();
                        }
                    }
                    else if (r == JOptionPane.CANCEL_OPTION)
                        return;
                }
                else
                    appl.quitter();
            }
        });
        panelBas.add(bQuitter);
        
        final JButton bEnregistrer = new JButton(new AbstractAction(Strings.get("bouton.Enregistrer")) {
            public void actionPerformed(ActionEvent e) {
                if (appl.enregistrerFichier(fichierXML)) {
                    if (appl.fsave != null)
                        fichierXML = appl.fsave;
                    appl.setModif(false);
                    JOptionPane.showMessageDialog(FenetreEdition.this, Strings.get("message.EnregistrementReussi"));
                }
            }
        });
        panelBas.add(bEnregistrer);
        
        /********************** A supprimer *****************************************/
        final JButton bTester = new JButton(new AbstractAction("tester") {
            public void actionPerformed(ActionEvent e) {
                EditeurFichierConfig.convertToString(doc);
            }
        });
        //panelBas.add(bTester);
        /************************************************************/
        
        add(panelBoutons, BorderLayout.NORTH);
        add(panelBas, BorderLayout.SOUTH);
        
        pack();
        final Dimension dim = getSize();
        final Dimension ecran = getToolkit().getScreenSize();
        setLocation((ecran.width - dim.width)/2, (ecran.height - dim.height)/2);
        setVisible(true);
    }
    
    
    private void autreConfig() {
        if (appl.fsave != null || fichierXML != null) {
            setVisible(false);
            File cheminFichier = null;
            if (appl.fsave != null)
                cheminFichier = appl.fsave;
            if (fichierXML != null)
                cheminFichier = fichierXML;
           new AutreConfig(FenetreEdition.this, doc, racine, cheminFichier);
        }
        else
            JOptionPane.showMessageDialog(FenetreEdition.this, Strings.get("message.SauverAvant"), Strings.get("titre.Erreur"), JOptionPane.ERROR_MESSAGE);        
    }
    
    private void editerEnregistrement() {
        new EditeurEnregistrement(this, doc, racine);
    }
    
    private void editerMenus() {
        new EditeurDeMenus(this, doc, racine, listeElements);
    }
    
    private void editerElements() {
        new EditeurDesElements(this, doc, racine, listeElements);
    }
    
    private void editerExports() {
        if (appl.fsave != null || fichierXML != null) {
            setVisible(false);
            File cheminFichier = null;
            if (appl.fsave != null)
                cheminFichier = appl.fsave;
            if (fichierXML != null)
                cheminFichier = fichierXML;
            new EditeurDesExports(this, doc, racine, cheminFichier);
        }
        else
            JOptionPane.showMessageDialog(FenetreEdition.this, Strings.get("message.SauverAvant"), Strings.get("titre.Erreur"), JOptionPane.ERROR_MESSAGE);
    }
    
    private void editerTextesG() {
        new EditeurDesTextes(this, doc, racine, listeElements);
    }
    
    
    // AFFICHAGE_ELEMENT
    private void enregistrerAffichageElement(final String nomElement) {
        Element affichage_element = Outils.getElementSelectionne(affichage_noeuds, "AFFICHAGE_ELEMENT", nomElement, "element");
        if (affichage_element == null) {
            affichage_element = doc.createElement("AFFICHAGE_ELEMENT");
            affichage_noeuds.appendChild(affichage_element);
            affichage_element.setAttribute("element", nomElement);
            affichage_element.setAttribute("type", "string");
            
            EditeurFichierConfig.setModif(true);
        }
    }
    
}
