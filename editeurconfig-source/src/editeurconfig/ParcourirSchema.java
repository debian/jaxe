/*
editeurconfig - Editeur de Fichiers de Config de Jaxe

Copyright (C) 2011 Observatoire de Paris-Meudon

Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier conform�ment aux dispositions de la Licence Publique G�n�rale GNU, telle que publi�e par la Free Software Foundation ; version 2 de la licence, ou encore (� votre choix) toute version ult�rieure.

Ce programme est distribu� dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans m�me la garantie implicite de COMMERCIALISATION ou D'ADAPTATION A UN OBJET PARTICULIER. Pour plus de d�tail, voir la Licence Publique G�n�rale GNU .

Vous devez avoir re�u un exemplaire de la Licence Publique G�n�rale GNU en m�me temps que ce programme ; si ce n'est pas le cas, �crivez � la Free Software Foundation Inc., 675 Mass Ave, Cambridge, MA 02139, Etats-Unis.
*/

package editeurconfig;

import javax.swing.*;
import org.w3c.dom.*;
import java.io.*;
import javax.swing.filechooser.FileFilter;
import java.net.URL;
import java.net.MalformedURLException;


/**
* Classe pour parcourir et s�l�ctionner un sch�ma
*/
public class ParcourirSchema {
    
    private EditeurFichierConfig appl;
    private Document doc;
    private Element racine;
    private Element langage;
    private Element fichier_schema;
    
    private String extension;
    
    public ParcourirSchema(final EditeurFichierConfig appl, final Document doc, final Element racine, final String extension) {
        this.appl = appl;
        this.doc = doc;
        this.racine = racine;
        this.extension = extension;
        
        afficher(extension);
    }
    
    private void afficher(final String extension) {
		JFileChooser jfc = new JFileChooser();
		jfc.setFileFilter(new FileFilter() {
			public boolean accept(File f) {
				if (f.isDirectory())
					return true;
				String s = f.getName();
				int i = s.lastIndexOf('.');

				if (i > 0 && i < s.length() - 1)
					if (s.substring(i + 1).toLowerCase().equals(extension))
						return true;

				return false;
			}

			public String getDescription() {
                return Strings.get("texte.Fichiers"+extension.toUpperCase());
			}
		});
		
        int returnValue = jfc.showOpenDialog(null);
        if (returnValue == JFileChooser.APPROVE_OPTION) {
			File f = jfc.getSelectedFile();
			charger(f);
		}
        else new ChoixDuSchema(appl, doc, racine);
    }
    
    
    /**
    * Charge le fichier du sch�ma, r�cup�re les �l�ments du sch�ma
    */
    private void charger(final File f) {
        ListeElements listeElements = null;
        try {
            URL urlFichier = f.toURI().toURL();
            listeElements = new ListeElements(urlFichier, null, null);
        } catch(MalformedURLException ex) {
            System.out.println("Erreur urlFichier "+ex);
        }
        enregistrerElementLangage();
        enregistrerElementSchema(f);
		//new FenetreEdition(appl, null, doc, racine, listeElements);
        new ChoixRacineN(appl, doc, racine, listeElements);
    }
    
    
    /**
    * Enregistre l'�l�ment LANGAGE dans le DOM
    */
    private void enregistrerElementLangage() {
        langage = doc.createElement("LANGAGE");
        racine.insertBefore(langage, Outils.premierEnfantDeNom(racine, "STRINGS"));
    }
    
    /**
    * Enregistre l'�l�ment FICHIER_SCHEMA dans le DOM
    */
    private void enregistrerElementSchema(final File f) {
        fichier_schema = doc.createElement("FICHIER_SCHEMA");
        fichier_schema.setAttribute("nom", f.getName());
        langage.appendChild(fichier_schema);
        
        EditeurFichierConfig.setModif(true);
    }
    
}


