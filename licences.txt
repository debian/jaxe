Jaxe : GPL

lib licences :
- avalon-framework-4.2.0.jar : Apache License Version 2.0
- batik-all-1.7.jar : Apache License Version 2.0
- commons-io-1.3.1.jar : Apache License Version 2.0
- commons-logging-1.0.4.jar : Apache License Version 2.0
- fop-0.95.jar : Apache License Version 2.0
- jazzy.jar : LGPL
- jing.jar : http://www.thaiopensource.com/relaxng/copying.html
- log4j-1.2.14.jar : Apache License Version 2.0
- resolver-1.2.jar : Apache License Version 2.0
- saxon9he.jar : Mozilla Public License Version 1.0
- serializer-2.7.1.jar : Apache License Version 2.0
- xalan-2.7.1.jar : Apache License Version 2.0
- xercesImpl-2.9.1.jar : Apache License Version 2.0
- xml-apis-1.3.04.jar : Apache License Version 2.0
- xml-apis-ext-1.3.04.jar : Apache License Version 2.0
- xmlgraphics-commons-1.3.1.jar : Apache License Version 2.0
- xsltc-2.7.1.jar : Apache License Version 2.0

plugin licences :
- xpages (for the XPAGES config) : GPL
- dost (for the DITA config) : Common Public License 1.0 or Apache License Version 2.0
